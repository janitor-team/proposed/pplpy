Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: pplpy
Upstream-Contact: Vincent Delecroix <vincent.delecroix@labri.fr>
Source: https://gitlab.com/videlec/pplpy

Files: *
Copyright: 2010-2014 Volker Braun  <vbraun.name@gmail.com>
           2011 Simon King <simon.king@uni-jena.de>
           2011-2017 Jeroen Demeyer <jdemeyer@cage.ugent.be>
           2012 Risan <ptrrsn.1@gmail.com>
           2013 Julien Puydt <julien.puydt@laposte.net>
           2013 Travis Scrimshaw <tscrim@ucdavis.edu>
           2015 André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
           2016 Jori Mäntysalo <jori.mantysalo@uta.fi>
           2016 Matthias Koeppe <mkoeppe@math.ucdavis.edu>
           2016-2017 Frédéric Chapoton <chapoton@math.univ-lyon1.fr>
           2016-2018 Vincent Delecroix <vincent.delecroix@labri.fr>
           2017-2018 Vincent Klein <vinklein@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: 2019 Tobias Hansen <thansen@debian.org>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.
