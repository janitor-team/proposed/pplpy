Source: pplpy
Section: python
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>, Julien Puydt <jpuydt@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python (>= 3.20180313),
 cython3 (>= 0.26),
 libgmp-dev,
 libmpfr-dev,
 libmpc-dev,
 libppl-dev,
 libpari-dev,
 libjs-mathjax,
 python3-all-dev,
 python3-cysignals-pari (>= 1.8.1),
 python3-gmpy2,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.5.1
Homepage: https://gitlab.com/videlec/pplpy
Vcs-Git: https://salsa.debian.org/science-team/pplpy.git
Vcs-Browser: https://salsa.debian.org/science-team/pplpy
Rules-Requires-Root: no

Package: python3-ppl
Architecture: any
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}, python3-cysignals-pari, python3-gmpy2
Description: Python interface to PPL -- Python 3
 A Python interface to the C++ Parma Polyhedra Library (PPL),
 which allows computations with polyhedra and grids, like mixed
 integer linear programming.
 .
 This package installs the library for Python 3.

Package: python-ppl-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}, libjs-mathjax
Description: Python interface to PPL -- documentation
 A Python interface to the C++ Parma Polyhedra Library (PPL),
 which allows computations with polyhedra and grids, like mixed
 integer linear programming.
 .
 This is the common documentation package.
